package lv.ms.homework.springjwtrest;

import lv.ms.homework.springjwtrest.domain.Customer;
import lv.ms.homework.springjwtrest.domain.User;
import lv.ms.homework.springjwtrest.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJwtApplicationTests {

    @Autowired
    UserRepository repo;

    @Test
    public void contextLoads() {
    }

    @Test
    @Transactional
    public void dataMappingTest() {
        User user = repo.findByUsername("john.user");
        List<Customer> customers = user.getCustomers();
        customers.forEach(Customer::getDebts);
    }
}
