package lv.ms.homework.springjwtrest.domain;

public enum Role {
    STANDARD_USER, ADMIN_USER
}
