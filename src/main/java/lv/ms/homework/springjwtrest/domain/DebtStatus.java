package lv.ms.homework.springjwtrest.domain;

public enum DebtStatus {
    PAID, NOT_PAID
}
