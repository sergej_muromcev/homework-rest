package lv.ms.homework.springjwtrest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lv.ms.homework.springjwtrest.requestobjects.DebtRequest;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "debt")
public class CustomerDebt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonIgnore
    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "amount")
    private Double amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DebtStatus status;

    public CustomerDebt() {
    }

    public CustomerDebt(Customer customer) {
        this.setCustomerId(customer.getId());
    }

    public CustomerDebt(Customer customer, DebtRequest debtRequest) {
        this.setCustomerId(customer.getId());
        this.copyInfoFrom(debtRequest);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public DebtStatus getStatus() {
        return status;
    }

    public void setStatus(DebtStatus status) {
        this.status = status;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public void copyInfoFrom(DebtRequest source) {
        setAmount(source.getAmount());
        setDueDate(source.getDueDate());
        setStatus(source.getStatus());
    }
}
