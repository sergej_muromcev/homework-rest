package lv.ms.homework.springjwtrest.controller;

import lv.ms.homework.springjwtrest.domain.Customer;
import lv.ms.homework.springjwtrest.domain.CustomerDebt;
import lv.ms.homework.springjwtrest.domain.Role;
import lv.ms.homework.springjwtrest.domain.User;
import lv.ms.homework.springjwtrest.requestobjects.CustomerRequest;
import lv.ms.homework.springjwtrest.requestobjects.DebtRequest;
import lv.ms.homework.springjwtrest.requestobjects.UserRequest;
import lv.ms.homework.springjwtrest.service.CustomerDebtService;
import lv.ms.homework.springjwtrest.service.CustomerService;
import lv.ms.homework.springjwtrest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.InvalidParameterException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/springjwt")
public class UserResourceController {
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerDebtService debtService;

    @RequestMapping(value = "/users/{username:.+}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN_USER', 'STANDARD_USER')")
    public User getUser(@PathVariable("username") String username, Authentication authentication) {
        if (authentication.getAuthorities().contains(Role.ADMIN_USER.name())
                || authentication.getPrincipal().toString().equals(username)) {
            return userService.findByUsername(username);
        } else {
            throw new AuthorizationServiceException("Sadly, you're unable to access this resource.");
        }
    }

    @RequestMapping(value = "/mycustomers", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN_USER', 'STANDARD_USER')")
    public Collection<Customer> getCustomers(Authentication authentication) {
        return userService.findByUsername(authentication.getPrincipal().toString()).getCustomers();
    }

    @RequestMapping(value = "/mycustomers/{customerid}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN_USER', 'STANDARD_USER')")
    public Collection<Customer> getCustomer(@PathVariable("customerid") Long customerId, Authentication authentication) {
        return userService.findByUsername(authentication.getPrincipal().toString())
                .getCustomers().stream().filter(x -> x.getId().equals(customerId)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/mycustomers", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('ADMIN_USER', 'STANDARD_USER')")
    public Customer createCustomer(@Valid @RequestBody CustomerRequest customerRequest, Authentication authentication) {
        User user = userService.findByUsername(authentication.getPrincipal().toString());
        if (customerRequest.getId() == null) { //create
            return customerService.createNew(user, customerRequest);
        } else { //update
            List<Customer> customers = user.getCustomers();
            Optional<Customer> tmp = customers.stream().filter(cust -> cust.getId().equals(customerRequest.getId())).findFirst();
            if (tmp.isPresent()) {
                Customer customer = tmp.get();
                customer.copyInfoFrom(customerRequest);
                return customerService.store(customer);
            } else {
                throw new InvalidParameterException("Requested customer cannot be updated by you.");
            }
        }
    }

    @RequestMapping(value = "/newuser", method = RequestMethod.POST)
    public User createCustomer(@Valid @RequestBody UserRequest userRequest) {
        User tmp = userService.findByUsername(userRequest.getUsername());
        if (tmp != null) {
            throw new InvalidParameterException("Please choose another username.");
        }

        return userService.createNew(userRequest);
    }

    @RequestMapping(value = "/customerdebts/{customerid}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN_USER', 'STANDARD_USER')")
    public Collection<CustomerDebt> getDebts(@PathVariable("customerid") Long customerId, Authentication authentication) {
        if (authentication.getAuthorities().contains(Role.ADMIN_USER.name())) {
            return customerService.load(customerId).getDebts();
        } else {
            User user = userService.findByUsername(authentication.getPrincipal().toString());
            Optional<Customer> customer = user.getCustomers().stream().filter(x -> x.getId().equals(customerId)).findFirst();
            return customer.isPresent() ? customer.get().getDebts() : null;
        }
    }

    @RequestMapping(value = "/customerdebts/{customerid}", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('ADMIN_USER', 'STANDARD_USER')")
    public CustomerDebt createDebt(@PathVariable("customerid") Long customerId, @Valid @RequestBody DebtRequest debtRequest, Authentication authentication) {
        Customer customer;
        if (authentication.getAuthorities().contains(Role.ADMIN_USER.name())) {
            customer = customerService.load(customerId);
        } else {
            User user = userService.findByUsername(authentication.getPrincipal().toString());
            Optional<Customer> userCust = user.getCustomers().stream().filter(x -> x.getId().equals(customerId)).findFirst();

            if (!userCust.isPresent()) {
                throw new InvalidParameterException("Requested customer cannot be updated by you.");
            }
            customer = userCust.get();
        }

        if (debtRequest.getId() == null) { //create
            return debtService.createNew(customer, debtRequest);
        } else { //update
            Optional<CustomerDebt> tmp = customer.getDebts().stream().filter(d -> d.getId().equals(debtRequest.getId())).findFirst();
            if (tmp.isPresent()) {
                CustomerDebt debt = tmp.get();
                debt.copyInfoFrom(debtRequest);
                return debtService.store(debt);
            } else {
                throw new InvalidParameterException("Requested debt cannot be updated by you.");
            }
        }
    }

}
