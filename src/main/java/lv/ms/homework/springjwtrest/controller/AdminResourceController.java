package lv.ms.homework.springjwtrest.controller;

import lv.ms.homework.springjwtrest.domain.Customer;
import lv.ms.homework.springjwtrest.domain.User;
import lv.ms.homework.springjwtrest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/springjwt")
public class AdminResourceController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public List<User> getUsers() {
        return userService.findAllUsers();
    }

    @RequestMapping(value = "/users/{username}/customers/", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public Collection<Customer> getCustomers(@PathVariable("username") String username) {
        return userService.findByUsername(username).getCustomers();
    }

}
