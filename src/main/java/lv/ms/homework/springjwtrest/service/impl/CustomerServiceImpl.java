package lv.ms.homework.springjwtrest.service.impl;

import lv.ms.homework.springjwtrest.domain.Customer;
import lv.ms.homework.springjwtrest.domain.User;
import lv.ms.homework.springjwtrest.repository.CustomerRepository;
import lv.ms.homework.springjwtrest.requestobjects.CustomerRequest;
import lv.ms.homework.springjwtrest.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Customer load(long id) {
        return customerRepository.findOne(id);
    }

    @Override
    public Customer store(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer createNew(User user, CustomerRequest customerRequest) {
        Customer tmp = new Customer(user, customerRequest);
        return customerRepository.save(tmp);
    }
}
