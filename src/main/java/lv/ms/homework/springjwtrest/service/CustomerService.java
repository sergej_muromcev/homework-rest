package lv.ms.homework.springjwtrest.service;

import lv.ms.homework.springjwtrest.domain.Customer;
import lv.ms.homework.springjwtrest.domain.User;
import lv.ms.homework.springjwtrest.requestobjects.CustomerRequest;

public interface CustomerService {
    Customer load(long id);

    Customer store(Customer customer);

    Customer createNew(User user, CustomerRequest customerRequest);
}
