package lv.ms.homework.springjwtrest.service.impl;

import lv.ms.homework.springjwtrest.domain.Customer;
import lv.ms.homework.springjwtrest.domain.CustomerDebt;
import lv.ms.homework.springjwtrest.repository.CustomerDebtRepository;
import lv.ms.homework.springjwtrest.requestobjects.DebtRequest;
import lv.ms.homework.springjwtrest.service.CustomerDebtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerDebtServiceImpl implements CustomerDebtService {

    @Autowired
    CustomerDebtRepository debtRepository;

    @Override
    public CustomerDebt load(long id) {
        return debtRepository.findOne(id);
    }

    @Override
    public CustomerDebt store(CustomerDebt customer) {
        return debtRepository.save(customer);
    }

    @Override
    public CustomerDebt createNew(Customer customer, DebtRequest debtRequest) {
        CustomerDebt tmp = new CustomerDebt(customer, debtRequest);
        return debtRepository.save(tmp);
    }
}
