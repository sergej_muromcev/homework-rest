package lv.ms.homework.springjwtrest.service.impl;

import lv.ms.homework.springjwtrest.domain.Role;
import lv.ms.homework.springjwtrest.domain.User;
import lv.ms.homework.springjwtrest.repository.UserRepository;
import lv.ms.homework.springjwtrest.requestobjects.UserRequest;
import lv.ms.homework.springjwtrest.service.UserService;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private static final DozerBeanMapper mapper = new DozerBeanMapper();
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public void store(User user) {
        userRepository.save(user);
    }

    @Override
    public User createNew(UserRequest user) {
        User tmp = mapper.map(user, User.class);
        tmp.setPassword(passwordEncoder.encode(user.getPassword()));
        tmp.setRoles(Collections.singletonList(Role.STANDARD_USER));
        return userRepository.save(tmp);
    }
}
