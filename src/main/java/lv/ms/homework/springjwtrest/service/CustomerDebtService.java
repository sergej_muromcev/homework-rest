package lv.ms.homework.springjwtrest.service;

import lv.ms.homework.springjwtrest.domain.Customer;
import lv.ms.homework.springjwtrest.domain.CustomerDebt;
import lv.ms.homework.springjwtrest.requestobjects.DebtRequest;

public interface CustomerDebtService {
    CustomerDebt load(long id);

    CustomerDebt store(CustomerDebt customer);

    CustomerDebt createNew(Customer customer, DebtRequest debtRequest);
}
