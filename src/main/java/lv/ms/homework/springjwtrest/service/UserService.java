package lv.ms.homework.springjwtrest.service;

import lv.ms.homework.springjwtrest.domain.User;
import lv.ms.homework.springjwtrest.requestobjects.UserRequest;

import java.util.List;

public interface UserService {
    User findByUsername(String username);

    List<User> findAllUsers();

    void store(User user);

    User createNew(UserRequest userRequest);
}
