package lv.ms.homework.springjwtrest.repository;

import lv.ms.homework.springjwtrest.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
