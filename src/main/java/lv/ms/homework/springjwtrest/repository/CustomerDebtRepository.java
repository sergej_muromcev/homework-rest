package lv.ms.homework.springjwtrest.repository;

import lv.ms.homework.springjwtrest.domain.CustomerDebt;
import org.springframework.data.repository.CrudRepository;

public interface CustomerDebtRepository extends CrudRepository<CustomerDebt, Long> {
}
