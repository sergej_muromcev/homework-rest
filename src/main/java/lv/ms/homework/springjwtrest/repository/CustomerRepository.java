package lv.ms.homework.springjwtrest.repository;

import lv.ms.homework.springjwtrest.domain.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
