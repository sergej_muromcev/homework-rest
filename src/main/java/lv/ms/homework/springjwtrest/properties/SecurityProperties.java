package lv.ms.homework.springjwtrest.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
@PropertySource("${security.properties.path}")
public class SecurityProperties {
    @Value("${security.signing-key}")
    private String signingKey;

    @Value("${security.encoding-salt}")
    private String encodingSalt;

    @Value("${security.security-realm}")
    private String securityRealm;

    @Value("${security.jwt.client-id}")
    private String jwtClientId;

    @Value("${security.jwt.client-secret}")
    private String jwtClientSecret;

    @Value("${security.jwt.grant-type}")
    private String jwtGrantType;

    @Value("${security.jwt.scope-read}")
    private String jwtScopeRead;

    @Value("${security.jwt.scope-write}")
    private String jwtScopeWrite;

    @Value("${security.jwt.resource-ids}")
    private String jwtResourceIds;

    public String getSigningKey() {
        return signingKey;
    }

    public String getEncodingSalt() {
        return encodingSalt;
    }

    public String getSecurityRealm() {
        return securityRealm;
    }

    public String getJwtClientId() {
        return jwtClientId;
    }

    public String getJwtClientSecret() {
        return jwtClientSecret;
    }

    public String getJwtGrantType() {
        return jwtGrantType;
    }

    public String getJwtScopeRead() {
        return jwtScopeRead;
    }

    public String getJwtScopeWrite() {
        return jwtScopeWrite;
    }

    public String getJwtResourceIds() {
        return jwtResourceIds;
    }
}
