-- Users
-- non-encrypted password: jwtpass
INSERT INTO app_user (id, username, password, email) VALUES (1, 'admin.admin', '291005ee790faf9ae1092d8f5580da410cdd00477ccdc291843c65a54bc898862ed215d8fb72cc37', 'Admin');
INSERT INTO app_user (id, username, password, email) VALUES (2, 'john.user', '291005ee790faf9ae1092d8f5580da410cdd00477ccdc291843c65a54bc898862ed215d8fb72cc37', 'john-the-user@company.domain');

INSERT INTO user_role(user_id, role) VALUES(1, 'STANDARD_USER');
INSERT INTO user_role(user_id, role) VALUES(1, 'ADMIN_USER');
INSERT INTO user_role(user_id, role) VALUES(2, 'STANDARD_USER');

-- Customer
INSERT INTO customer (id, user_id, name, surname, email, address, personal_id) VALUES (1, 2, 'Doughlas', 'McDebter', 'doughlas@memorized', 'Riga, some street 52-27', '12345-6789');
INSERT INTO customer (id, user_id, name, surname, email, address, personal_id) VALUES (2, 2, 'Mary', 'Innocent', 'mary.inno@cent.com', 'Riga, center square 2', '0987654321');

-- Debts
INSERT INTO debt (id, customer_id, due_date, amount, status) VALUES (1, 1, '2018-12-31', 100.57, 'NOT_PAID');