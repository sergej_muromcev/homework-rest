CREATE TABLE app_user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  username varchar(255) NOT NULL UNIQUE,
  password varchar(255) NOT NULL,
  email varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE user_role (
  user_id bigint(20) NOT NULL,
  role varchar(20) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES app_user (id)
);

CREATE TABLE customer (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    user_id bigint(20) NOT NULL,
    name varchar(255),
    surname varchar(255),
    email varchar(255),
    address varchar(255),
    personal_id varchar(255),
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES app_user (id)
);

CREATE TABLE debt (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    customer_id bigint(20) NOT NULL,
    due_date date NOT NULL,
    amount double NOT NULL,
    status varchar(10) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (customer_id) REFERENCES customer (id)
);

