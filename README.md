# springboot-jwt

## To run the application
Use one of the several ways of running a Spring Boot application. Below are just three options:

1. Build using maven goal: `mvn clean package` and execute the resulting artifact as follows `java -jar springboot-jwt-0.0.1-SNAPSHOT.jar` or
2. On Unix/Linux based systems: run `mvn clean package` then run the resulting jar as any other executable `./springboot-jwt-0.0.1-SNAPSHOT.jar`
3. Build and start as a Docker container. Instructions at: [README](src/main/docker/README.md)

## To test the application

 ### First you will need the following basic pieces of information:

 * client: jwtclientid
 * secret: M9uKqD8NEJA3H
 * Non-admin username and password: john.user and jwtpass
 * Admin user: admin.admin and jwtpass

 ### Generate an user access token

   For this specific application, to generate an access token for the non-admin user john.doe, run:
    
    $ curl jwtclientid:M9uKqD8NEJA3H@localhost:8080/oauth/token -d grant_type=password -d username=john.user -d password=jwtpass
   
   
 ### Use the token to access resources through your RESTful API

   * Access user content

    curl  http://localhost:8080/springjwt/mycustomers/ -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiand0cmVzb3VyY2VpZCJdLCJ1c2VyX25hbWUiOiJqb2huLnVzZXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTI0Mjk2NzExLCJhdXRob3JpdGllcyI6WyJTVEFOREFSRF9VU0VSIl0sImp0aSI6IjdiMGJhZDEyLTQ0NjUtNDdlNS04YmNkLWY3YzJjZDliMjFjYyIsImNsaWVudF9pZCI6Imp3dGNsaWVudGlkIn0.cM9XztHl7_PpDkOdQLBs-fEzl1FauGCEdc0wweCEY4c"
 